import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import reduxThunk from "redux-thunk";
import registerServiceWorker from './registerServiceWorker';
import PageRouter from './Components/Router'
import reducers from './Reducers'
import { firebaseApp } from './Config/firebase';

import './index.css';

const store = createStore(reducers, {}, applyMiddleware(reduxThunk));

ReactDOM.render(
  <Provider store={store}>
    <PageRouter />
  </Provider>, 
    document.getElementById('root')
);
registerServiceWorker();
