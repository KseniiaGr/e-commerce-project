import Chocolate from './Images/chocolate.png.png';
import Cookies from './Images/Cookies.png.png';
import Doughnut from './Images/Doughnut.png.png';
import Sweets from './Images/Sweets.png.png';
import Coffee from './Images/Coffee.png.png';
import Tea from './Images/Tea.png.png';
import CocaCola from './Images/pepsi.png.png';
import Joystick from './Images/Joystick.png.png';
import Mouse from './Images/Mouse.png.png';
import { ADD_PRODUCT, FETCH_PRODUCTS } from '../Actions/Types';
import { fetchProducts } from '../Actions/AddProductAction';


// const INITAL_STATE = {
//   products: [{
//     "Code": "BAcyDyQwcXX",
//     "Category": "Food",
//     "Name": "Milk Chocolate",
//     "Description": "Very Milky good af chocolate",
//     "Price": 1,
//     "Image": Chocolate,
//     "InStock": 300
//   },{
//     "Code": "BAcyDyQwcBB",
//     "Category": "Food",
//     "Name": "White Chocolate",
//     "Description": "Consists of cocoa butter, sugar and milk solids.So it is technically not a chocolate ",
//     "Price": 2,
//     "Image": Chocolate,
//     "InStock": 250
//   },{
//     "Code": "BBcyDyQwcXX",
//     "Category": "Food",
//     "Name": "Dark Chocolate",
//     "Description": "Our strongest Belgian dark chocolate, crafted from a blend of African and Caribbean cocoa beans.",
//     "Price": 5,
//     "Image": Chocolate,
//     "InStock": 301
//   },{
//     "Code": "BAkiDyQwcXX",
//     "Category": "Food",
//     "Name": "Chocolate Chip Cookies",
//     "Description": "Chocolate chip cookies America's single greatest contribution to world",
//     "Price": 2,
//     "Image": Cookies,
//     "InStock": 200
//   },{
//     "Code": "AAkiDyQwcXX",
//     "Category": "Food",
//     "Name": "Doughnut",
//     "Description": "Krispy Kreme Doughnuts creates delicious, melt-in-your-mouth pieces of donut joy.",
//     "Price": 2.50,
//     "Image": Doughnut,
//     "InStock": 150
//   },{
//     "Code": "CCkiDyQwcXX",
//     "Category": "Food",
//     "Name": "Sweets",
//     "Description": "Confection that features sugar as a principal ingredient.",
//     "Price": 0.50,
//     "Image": Sweets,
//     "InStock": 500
//   },{
//     "Code": "DCkiDyQwcXX",
//     "Category": "Beverages",
//     "Name": "Coffee",
//     "Description": "100% Arabica",
//     "Price": 2.70,
//     "Image": Coffee,
//     "InStock": 240
//   },{
//     "Code": "EEkiDyQwcXX",
//     "Category": "Beverages",
//     "Name": "Tea",
//     "Description": "English breakfast tea is a traditional blend of teas originating from Assam, Ceylon, and Kenya.",
//     "Price": 1.50,
//     "Image": Tea,
//     "InStock": 230
//   },{
//     "Code": "FFkiDyQwcXX",
//     "Category": "Beverages",
//     "Name": "Coka-Cola",
//     "Description": " Originally intended as a patent medicine.",
//     "Price": 0.90,
//     "Image": CocaCola,
//     "InStock": 109
//   },{
//     "Code": "GGkiDyQwcXX",
//     "Category": "Games",
//     "Name": "PS4 controller",
//     "Description": "A responsive DualShock controller will let you own FIFA 19 like a Premier League player.",
//     "Price": 50,
//     "Image": Joystick,
//     "InStock": 30
//   },{
//     "Code": "hHkiDyQwcXX",
//     "Category": "Games",
//     "Name": "Logitech G903",
//     "Description": "Stellar wireless performance",
//     "Price": 78,
//     "Image": Mouse,
//     "InStock": 50
//   }]
//   };

export default (state = {}, action) =>{
  switch (action.type){
    case FETCH_PRODUCTS:
    if (!action.payload){
      return state;
    }
      return  action.payload;
    case ADD_PRODUCT:
      const products = [...state.products];
      products.push(action.payload)
      return {...state, products }
    default:
      return state;
  }
};
