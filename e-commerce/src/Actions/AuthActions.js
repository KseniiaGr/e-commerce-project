import { SIGNED_IN } from './Types';

export function logUser(email){
  const action = {
    type: SIGNED_IN,
    email
  }
  return action;
}
