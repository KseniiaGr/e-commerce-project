import { REMOVE_FROM_BASKET } from './Types';

export function removeItem(item){
  const action = {
    type: REMOVE_FROM_BASKET ,
    item
  }
  return action;
}
