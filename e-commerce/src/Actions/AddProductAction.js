import { FETCH_PRODUCTS, FETCH_FEEDBACK, FETCH_ADMINS } from './Types';
import { gameStopRef, feedbackRef, adminsRef } from '../Config/firebase'

export const addProducts = product => async dispatch => {
  gameStopRef.push().set(product);
};

export const removeStock = product => async dispatch => {
  if(window.confirm("Are you sure you want to delete this item?")) {
    gameStopRef.child(product).remove();
  }
}

export const removeFeedback = feedback => async dispatch => {
  if(window.confirm("Are you sure you want to delete this item?")) {
    feedbackRef.child(feedback).remove();
  }
}

export const addFeedback = feedback => async dispatch => {
  feedbackRef.push().set(feedback);
};

export const addStock = (product, inStock) => async dispatch => {
  inStock ++;
  gameStopRef.child(product).child("InStock").set(inStock);
}

export const minusStock = (product, inStock) => async dispatch => {
  if(inStock > 0 ){
  inStock --;
  gameStopRef.child(product).child("InStock").set(inStock);
  }
}
// export const addProducts = (product) => ({
//   type: ADD_PRODUCT,
//   payload: product
// });

export const fetchProducts = () => async dispatch => {
  gameStopRef.on("value", snapshot => {
    dispatch({
      type: FETCH_PRODUCTS,
      payload: snapshot.val()
    });
  });
};

export const fetchFeedback = () => async dispatch => {
  feedbackRef.on("value", snapshot => {
    dispatch({
      type: FETCH_FEEDBACK,
      payload: snapshot.val()
    });
  });
};

export const fetchAdmins = () => async dispatch => {
  adminsRef.on("value", snapshot => {
    dispatch({
      type: FETCH_ADMINS,
      payload: snapshot.val()
    });
  });
};

