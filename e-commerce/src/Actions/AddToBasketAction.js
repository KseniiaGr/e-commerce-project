import { ADD_TO_BASKET } from './Types';

export function addToBasket(product){
  const action = {
    type: ADD_TO_BASKET,
    product
  }
  return action;
}
