// import * as firebase from 'firebase';

// const config = {
//   apiKey: "AIzaSyDg9rEG4s5Y7HJyTXx6RuDFVLkuhmUCFD8",
//   authDomain: "game-spot-c08de.firebaseapp.com",
//   databaseURL: "https://game-spot-c08de.firebaseio.com",
//   projectId: "game-spot-c08de",
//   storageBucket: "game-spot-c08de.appspot.com",
//   messagingSenderId: "547691930205"
//   };

// export const firebaseApp = firebase.initializeApp(config);

import * as firebase from "firebase";

import { FirebaseConfig } from "../Config/keys";
firebase.initializeApp(FirebaseConfig);

const databaseRef = firebase.database().ref();
export const gameStopRef = databaseRef.child("products");
export const feedbackRef = databaseRef.child("feedback");
export const adminsRef = databaseRef.child("admins");

 export default firebase;
