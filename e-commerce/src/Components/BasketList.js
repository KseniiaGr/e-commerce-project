import "./CSS/BasketBar.css";
import React, { Component } from "react";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as removeFromBasket from '../Actions/RemoveFromBasketAction';
import { Row, Col, Icon, Button } from "react-materialize";

const BasketList = ({ items, total, removeItem }) => {
  const itemElements = items.map((item) => {
    return (
       <div className="ItemContainer" key={item.Code}>
        <Row>
          <Col s={3}>
            <img src={item.Image} />
          </Col>
          <Col s={6}>
           <Row>
              <h4>
                {item.Name}
              </h4>
            </Row>
            <Row>
              <h4>
                £ {item.Price}
              </h4>
            </Row>
          </Col>
          <Col
            className="right" s={3}
            onClick={() => removeItem(item)}>
          <a className="noStyle">
            <Icon
              >close
            </Icon>
          </a>
          </Col>
        </Row>
      </div>
    )
  });


  return (
      <div>
        {itemElements}
      </div>
  )
}

const mapStateToProps = (state) => {
  return {
    basket: state.basket,
    basketTotal: state.basketTotal
  };
}
export default connect(mapStateToProps, removeFromBasket)(BasketList);
