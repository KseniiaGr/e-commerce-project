import React from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
import ReduxThunk from 'redux-thunk';
import rootReducer from '../Reducers/index';
import App from './App';
import ProductPage from './ProductPage'
import CheckoutPage from './CheckoutPage'
import AccountSetup from './AccountSetup';
import CreateProduct from './CreateProduct';
import AdminStockView from './AdminStockView';
import FeedBackForm from './FeedBackForm';

const store = createStore(rootReducer, {}, applyMiddleware(ReduxThunk));

const PageRouter = () => (
  <Provider store={store}>
     <Router>

         <Switch>
         <Route exact path="/admin/stockview" component={AdminStockView} />
          <Route exact path="/account/setup" component={AccountSetup} />
          <Route exact path="/basket/checkout" component={CheckoutPage}/>
          <Route exact path="/product" component={ProductPage}/>
          <Route exact path="/" component={App}/>
        </Switch>

    </Router>
  </Provider>
);

export default PageRouter;
