import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchFeedback } from "../Actions/AddProductAction";
import { removeFeedback } from '../Actions/AddProductAction';
import {Table} from 'react-materialize';

const TableRow = ({ id, name, email, feedback, onRemove}) => {
  return (
    <tr key={id}>
      <td>{name}</td>
      <td>{email}</td>
      <td>{feedback}</td>
      <td>
        <button onClick={() => onRemove(id)} className="quantityBtn">
          X
        </button>
      </td>
    </tr>
  );
}

class FeedbackList extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.fetchFeedback();
  }

  feedbackRemove = (id) => {
    console.log(id);
    this.props.removeFeedback(id);
  }

   render() {
    const feedback = this.props.feedback;
    console.log(feedback)

    const feedbackRows = Object.keys(feedback).map((key) => {
      return (
        <TableRow
          id={key}
          name={feedback[key].Name}
          email={feedback[key].Email}
          feedback={feedback[key].Feedback}
          onRemove={this.feedbackRemove}
        />
      );
    });

    return (
       <Table>
       <thead>
         <tr>
           <th data-field="name">Name</th>
           <th data-field="email">Category</th>
           <th data-field="feedback">Price</th>
           <th data-field="remove">Remove</th>
         </tr>
       </thead>
       <tbody>
         {feedbackRows}
       </tbody>
     </Table>
    );
  }
}

const mapStateToProps = ({ feedback }) => {
  return {
    feedback
  };
};

export default connect(
  state => ({
    feedback: state.feedback
  }),
  { fetchFeedback, removeFeedback }
)(FeedbackList);