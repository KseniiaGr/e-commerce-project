import React, { Component } from 'react';
import {Col, Row, Modal} from 'react-materialize';
import { Card, Button, CardImg, CardTitle, CardText, CardBody } from 'reactstrap';
import './CSS/ProductCard.css';
import ProductPage from './ProductPage';

class ProductCard extends Component{

  render(){

    const { Category, Code, Description, Image, Name, Price } = this.props.product;

    return(
      <Col s={3}>
        <Card>
          <Row>
            <CardImg top width="100%" src={Image} alt={Name}/>
          </Row>
          <CardBody>
            <CardTitle>{Name}</CardTitle>
            <CardText>£ {parseFloat(Price).toFixed(2)}</CardText>
            <Modal
              trigger={<Button>Info</Button>}>
                <ProductPage product={this.props.product}/>
              </Modal>
          </CardBody>
        </Card>
      </Col>
    )
  }
}

export default ProductCard;
