import React, { Component } from 'react';
import {Row, Input, Col, Button, Toast} from 'react-materialize';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Header from './Header';
import './CSS/CheckoutPage.css';
import BasketList from './BasketList';

class CheckoutPage extends Component {
  render() {
    return (
      <div className="App">
        <div className="Header">
          <Header />
        </div>
        <Row>
        <Col s={6}>
        <div className="AddressInput">
          <h1>Shipping Address:</h1>
           <form name="form" >
             <ul>
               <li>
                 <label>Apt Number: </label>
                 <input
                   type="text"
                   name="aptNum"
                   placeholder="000"
                 />
              </li>
              <li>
                <label>Street: </label>
                <input
                  type="text"
                  name="street"
                  placeholder="Redwood"
                />
               </li>
               <li>
                 <label>City: </label>
                 <input
                   type="text"
                   name="city"
                   placeholder="London"
                 />
                </li>
                <li>
                  <label>Post Index: </label>
                  <input
                    type="text"
                    name="index"
                    placeholder="L1 12L"
                  />
                 </li>
             </ul>
           </form>
      </div>
    </Col>
    <Col s={6}>
      <div className="BasketPreview">
        <h1> Basket </h1>
        <div className="checkoutContent">
          <BasketList
            items={this.props.basket.basket}
            total={this.props.basket.basketTotal}
          />
        </div>
        <h5>
            Total: £{this.props.basket.basketTotal}
          </h5>
      </div>
    </Col>
    </Row>
    <Row>
      <Col s={6}>
      <div className="PaymentInput">
        <h1>Payment Method:</h1>
         <form name="form" >
           <ul>
           <Row>
             <li>
              <Input
                name='card'
                type='radio'
                value='visa'
                label='visa'
              />
              <Input
                name='card'
                type='radio'
                value='MasterCard'
                label='Master Card'
              />
            </li>
          </Row>
             <li>
               <label>Name On Card: </label>
               <input
                 type="text"
                 name="nameOnCard"
                 placeholder="Joohn Doe"
               />
            </li>
            <li>
              <label>Card Number: </label>
              <input
                type="text"
                name="cardNum"
                placeholder="0000 0000 0000 0000"
              />
             </li>
             <li>
               <label>Expiry Date: </label>
               <input
                 type="text"
                 name="expiryDate"
                 placeholder="MM/YY"
               />
             </li>
              <li>
               <label>Security Code: </label>
               <input
                 type="text"
                 name="secutityCode"
                 placeholder="000"
               />
            </li>
           </ul>
         </form>
         <div className="submitButton">
             <Button
               onClick={ () => window.Materialize.toast("Your order is on the way!", 1000)}
               waves='light'>Checkout and Pay</Button>
         </div>
    </div>
  </Col>
</Row>

      </div>

    );
  }
}

const mapStateToProps = (state) => {
  return {
    basket: state.basket,
  };
}
export default connect(mapStateToProps, null)(CheckoutPage);
