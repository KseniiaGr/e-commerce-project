import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchAdmins } from '../Actions/AddProductAction';
import firebase from '../Config/firebase'
import { Redirect } from 'react-router-dom';
import AdminStockView from './AdminStockView';
import {Row, Input, Col, Button, Icon} from 'react-materialize';
import Header from './Header';
import './CSS/AccountSetup.css';

class AccountSetup extends Component {
  constructor(props){
     super(props);
     this.state = {
       isAdamin: false,
       email: '',
       password: '',
       redirectToReferrer: false,
       error: {
         message: ''
       }
     }
   }

   componentDidMount() {
     this.props.fetchAdmins();
   }

   authHandler = async authData => {
    localStorage.setItem("authData", authData); 
    console.log(authData.user.uid);

     if(authData.user.uid === "jDk32rsaaFRAT8eMCkO3aWGZSbG3"){
       this.setState(
         {
           isAdmin: true,
         }
       )
     }
     this.setState(
       {
         redirectToReferrer: true
       }
     )
   }

   signIn(){
      const {email, password } = this.state;
      firebase.auth().signInWithEmailAndPassword(email, password)
      .then(
        this.authHandler
      )
      .catch(error =>{
        this.setState({error})
      })
    }

    signUp(){
   const {email, password } = this.state;
   firebase.auth().createUserWithEmailAndPassword(email, password)
   .then( ()=> {
     this.setState({ redirectToReferrer: true })
   })
   .catch(error =>{
     this.setState({error})
   })
 }


  render() {

    const { redirectToReferrer } = this.state

   if(redirectToReferrer){
     if(this.state.isAdmin){
      return(
        <Redirect to={'/admin/stockview'}/>
      );
    } else {
      return(
        <Redirect to={'/'} />
      )
    }
   }

    return (
      <div>
        <Header />
        <Row>
          <Col s={4}></Col>
          <h3 s={4}>Login / Register</h3>
          <Col s={4}></Col>
        </Row>
        <Row>
          <Col s={4}></Col>
          <Input
            placeholder="Email"
            s={4}
            onChange={event => this.setState({email: event.target.value})}
            name="email"
            type="email"
            className="validate"
          />
          <Col s={4}></Col>
        </Row>
        <Row>
          <Col s={4}></Col>
          <Input
            type="password"
            placeholder="Password"
            s={4}
            onChange={event => this.setState({password: event.target.value})}
            name="password"
            className="validate"
          />
          <Col s={4}></Col>
        </Row>
        <Row>
          <Col s={5}></Col>
          <Button
            id="regBtn"
            className="blue"
            waves='light'
            type="submit"
            onClick={() => this.signIn()}
            >Login<Icon left>done</Icon></Button>
          <Button
            id="regBtn"
            className="red"
            waves='light'
            onClick={() => this.signUp()}
            >Register<Icon right>create</Icon></Button>
            <div className="row">
             <div className="ErrorMsg">{this.state.error.message}</div>
           </div>
          <Col s={5}></Col>
        </Row>
      </div>
    );
  }
}



const mapStateToProps = ({ admins }) => {
  return {
    admins
  };
};

export default connect(
  mapStateToProps,
{ fetchAdmins }
)(AccountSetup);
