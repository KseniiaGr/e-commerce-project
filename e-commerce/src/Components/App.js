import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../Actions";
import { Row, Col } from "react-materialize";
import { fetchProducts } from "../Actions/AddProductAction";
import "./CSS/App.css";
import Header from "./Header";
import Filter from "./Filter";
import ProductList from "./ProductList";
import BasketBar from "./BasketBar";
import FormBar from "./Formbar";
import _ from "lodash";

class App extends Component {
  state = {
    hidden: true,
    formHidden:true,
    products: {},
    filter: false

  };

  componentDidMount() {
    this.props.fetchProducts();
  };

  
  static getDerivedStateFromProps(props, state){
    if(!state.filter){
      if(props.products !== state.products){
        return {
          products: props.products
        };
      }
        return null;
    }
  }

  updateFilterByCatagory = event => {
    let productFilter = this.props.products;
    switch (event.target.value) {
      case "3":
        productFilter = _.filter(productFilter, {
          Category: "Games"
        });
        this.setState(
          {
            products: productFilter,
            filter: true
          }
        );
        console.log("Fired")
        break;
      case "1":
        productFilter = _.filter(productFilter, { Category: "Food" });
        this.setState(
          {
            products: productFilter,
            filter: true
          }
        );
        break;
      case "2":
        productFilter = _.filter(productFilter, {
          Category: "Beverages"
        });
        this.setState(
          {
            products: productFilter,
            filter: true
          }
        );
        break;
      case "4":
        this.setState(
          {
            products: productFilter,
            filter: true
          }
        )
        break;
    }
  };

  sortBy = event => {
    let productFilter = this.props.products;
    switch (event.target.value) {
      case "1":
      this.setState(
        {
          products: productFilter,
          filter: true
        }
      );
      case "2":
        productFilter = _.sortBy(productFilter, ["Price"]);
        this.setState(
          {
            products: productFilter,
            filter: true
          }
        );
        break;
      case "3":
        productFilter = _.sortBy(productFilter, ["Price"]).reverse();
        this.setState(
          {
            products: productFilter,
            filter: true
          }
        );
    }
  };

  maxPrice = (e, maxPrice) => {
    let productFilter = this.props.products;
    productFilter = _.filter(productFilter, function(product) {
      return product.Price <= maxPrice;
    });
    this.setState(
      {
        products: productFilter,
        filter: true
      }
    );
    
  };

  handleBasketBarClose = () => {
    this.setState({
      ...this.state,
      hidden: true,
    });
  };

  handleBasketBarOpen = () => {
    this.setState({
      ...this.state,
      hidden: false,
      formHidden:true
    });
  };

  handleFormBarOpen = () => {
    this.setState(
      {
        ...this.state,
        formHidden:false,
        hidden:true
      }
    )
  }

  handleFormBarClose = () => {
    this.setState(
      {
        ...this.state,
        formHidden:true
      }
    )
  }

  render() {
    if (this.state.hidden && this.state.formHidden) {
      return (
        <div className="App">
          <Header handleBasketBar={this.handleBasketBarOpen} handleFormBarOpen={this.handleFormBarOpen}/>
          <Row>
            <Col s={2}>
              <Filter
                updateFilterByCatagory={this.updateFilterByCatagory}
                sortBy={this.sortBy}
                maxPrice={this.maxPrice}
              />
            </Col>
            <Col s={10}>
              <ProductList products={this.state.products} />
            </Col>
          </Row>
        </div>
      );
    } else if(!this.state.formHidden) { 
      return(
      <div className="App">
          <Header handleBasketBar={this.handleBasketBarOpen} handleFormBarOpen={this.handleFormBarOpen} />
          <Row>
            <Col s={2}>
              <Filter
                updateFilterByCatagory={this.updateFilterByCatagory}
                sortBy={this.sortBy}
                maxPrice={this.maxPrice}
              />
            </Col>
            <Col s={10}>
              <ProductList products={this.state.products} />
            </Col>
            <FormBar handleFormBar={this.handleFormBarClose}/>
          </Row>
        </div>
      )
    } if(!this.state.hidden) {
      return (
        <div className="App">
          <Header handleBasketBar={this.handleBasketBarOpen} handleFormBarOpen={this.handleFormBarOpen} />
          <Row>
            <Col s={2}>
              <Filter
                updateFilterByCatagory={this.updateFilterByCatagory}
                sortBy={this.sortBy}
                maxPrice={this.maxPrice}
              />
            </Col>
            <Col s={10}>
              <ProductList products={this.state.products} />
            </Col>
            <BasketBar handleBasketBar={this.handleBasketBarClose} />
          </Row>
        </div>
      );
    }
  }
};

const mapStateToProps = ({ products }) => {
  return {
    products
  };
};

export default connect(
  mapStateToProps,
  { fetchProducts }
)(App);
