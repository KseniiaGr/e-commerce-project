import React, { Component } from "react";
import { Col, Row, Toast } from "react-materialize";
import { Button } from "reactstrap";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as addToBasket from "../Actions/AddToBasketAction";

import "./CSS/ProductPage.css";

class productPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      Name: this.props.product.Name,
      Description: this.props.product.Description,
      Price: this.props.product.Price,
      Image: this.props.product.Image,
      Category: this.props.product.Category,
      Code: this.props.product.Code
    };
  }

  onClick = e => {
    window.Materialize.toast("Added To Basket!", 1000);
    this.props.actions.addToBasket(this.state);
  };

  render() {
    const {
      InStock,
      Category,
      Code,
      Description,
      Image,
      Name,
      Price
    } = this.props.product;
    if (InStock > 0) {
      return (
        <div className="Product-part">
          <Row>
            <Col s={4}>
              <img src={Image} alt="img" />
            </Col>
            <Col s={8}>
              <div className="Text-part">
                <h3>{Name}</h3>
                <h5>Price: £{Price}</h5>
                <p>{Category}</p>
                <p>{Description}</p>
                <p>
                  Stock Level:
                  {InStock}
                </p>
              </div>
            </Col>
          </Row>
          <Row>
            <Col s={6} />
            <Col s={6}>
              <div className="Buttons">
                <Button onClick={this.onClick} color="success">
                  Add to Basket
                </Button>
              </div>
            </Col>
          </Row>
        </div>
      );
    } else {
      return (
        <div className="Product-part">
          <Row>
            <Col s={4}>
              <img src={Image} alt="img" />
            </Col>
            <Col s={8}>
              <div className="Text-part">
                <h3>{Name}</h3>
                <h5>Price: £{Price}</h5>
                <p>{Category}</p>
                <p>{Description}</p>
                <p>
                  Stock Level:
                  {InStock}
                </p>
              </div>
            </Col>
          </Row>
          <Row>
            <Col s={6} />
            <Col s={6}>
              <div className="Buttons">
                <Button color="success">
                  Out of Stock
                </Button>
              </div>
            </Col>
          </Row>
        </div>
      );
    }
  }
}

export default connect(
  state => ({
    basket: state.basket
  }),
  dispatch => ({
    actions: bindActionCreators(addToBasket, dispatch)
  })
)(productPage);
