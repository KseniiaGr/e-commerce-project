import React, { Component } from "react";
import './CSS/Header.css';
import logo from '../Resources/Images/myLogo.png.png';
import {Row, Col, Icon} from 'react-materialize';
import { Link } from 'react-router-dom';

class Header extends Component {
  render() {
    return (
      <div>
        <header className="Header-header">
          <Row>
            <Col s={3}>
              <img src={logo} className="Header-logo" alt="logo" />
            </Col>
            <Col s={4}>
            <Link to={'/'}>
              <h1 className="Header-title">GameSpot</h1>
            </Link>
            </Col>
            <Col s={2}/>
            <Col s={3} className="Header-icons">
              <a className="noStyle" onClick={this.props.handleBasketBar}>
                <Icon medium className="Header-icon">shopping_basket</Icon>
              </a>
              <Link to={'/account/setup'}>
                <Icon medium className="Header-icon">account_box</Icon>
              </Link>
              <a className="noStyle" onClick={this.props.handleFormBarOpen}>
                <Icon medium className="Header-icon">rate_review</Icon>
              </a>
            </Col>
          </Row>
        </header>
      </div>
    );
  }
}

export default Header;
