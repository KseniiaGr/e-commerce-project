import React, { Component } from 'react';
import { Row, Col } from 'react-materialize';
import CreateProduct from './CreateProduct';
import StockTable from './StockTable';
import FeedbackList from './FeedbackList';
import Header from './Header';

class AdminStockView extends Component {
  render() {
    return (
      <div>
        <Header/>
        <Row>
          <Col s={6}>
            <CreateProduct />
          </Col>
          <Col s={6}>
            <StockTable />
          </Col>
        </Row>
        <Row className="center">
          <h5>User Feed Back Forms</h5>
          <div className="centerTable">
          <FeedbackList />
          </div>
        </Row>
      </div>
    )
  }
}

export default AdminStockView;