import React, {Component} from 'react'; 
import './CSS/Filter.css';
import {Row, Icon, Input} from 'react-materialize';

class Filter extends Component{
  constructor() {
    super();
    this.state = {
      catagory: 4,
      sortBy: 1,
      maxPrice: 100
    };
    this.handleSortBy = this.handleSortBy.bind(this);
    this.handleCatagory = this.handleCatagory.bind(this);
    this.handleMaxPrice = this.handleMaxPrice.bind(this);
  }



  handleMaxPrice(e){
    this.setState(
      {
        ...this.state,
        maxPrice: e.target.value
      }
    )
    this.props.maxPrice(e, this.state.maxPrice);
  }
  
  handleCatagory(e) {
    console.log(e.target.value)
    console.log(this.state);
    this.setState(
      {
        ...this.state,
        catagory: e.target.value
      }
    );
    this.props.updateFilterByCatagory(e);
  }
 
  handleSortBy(e){
    this.setState(
      {
        ...this.state,
        sortBy: e.target.value
      }
    );
    this.props.sortBy(e);
  }

  render(){
    return(
      <div className="Filter">
        <Row>
        <h5><Icon>filter_list</Icon> Filter</h5>
        </Row>
        <div>
          <Row>
            <div className="selectcontainer">
              <Input s={12} type='select' label="Catagory" onChange={this.handleCatagory} value={this.state.catagory} >
                <option value='1'>Food</option>
                <option value='2'>Beverages</option>
                <option value='3'>Games</option>
                <option value='4'>All</option>
              </Input>
            </div>
          </Row> 
          
          <div className="slidecontainer">
            <Row>
              <h6>Max Price:</h6>
              <Input type="range" onChange={this.handleMaxPrice} min='0' max="100" value={this.state.maxPrice}  className="slider" id="minPrice"/>
            </Row>
          </div>
          <div className="selectcontainer">
            <Input s={12} type='select' label="Sort By" onChange={this.handleSortBy} value={this.state.sortBy} defaultValue='1'>
              <option value='1'>Relevance</option>
              <option value='2'>Price - Low to High</option>
              <option value='3'>Price - High to Low</option>
            </Input>
          </div>
        </div>
      </div>
    )
  }
}

export default Filter;