import React, { Component } from "react";
import { Row, Col, Icon, Button } from "react-materialize";
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Link } from 'react-router-dom';
import ProductCard from './ProductCard';
import * as removeFromBasket from '../Actions/RemoveFromBasketAction';
import "./CSS/BasketBar.css";
import BasketList from './BasketList';

class BasketBar extends Component {

  constructor(props){
    super(props);
    this.state = {
      displayNavBar: "BasketBar",
      spinning: "noStyle spinningIn",
      total: 0
    }
  }


  handleClose = () => {
    // Add exit animation
    this.setState(
      {
        displayNavBar: "BasketBar Exit",
        spinning: "noStyle spinningOut"
      }
    )
    setTimeout(this.closing, 900);
  };

  closing = () =>{
    this.props.handleBasketBar()
  }

  render() {
    return (
      <div className={this.state.displayNavBar}>
        <Row className="closeButton">
          <a onClick={this.handleClose} className="noStyle">
            <Icon className={this.state.spinning} medium>
              close
            </Icon>
          </a>
        </Row>
        <div className="basketContent">
          <BasketList
            items={this.props.basket.basket}
            total={this.props.basket.basketTotal}
            removeItem={this.props.removeItem}
            {...this.props}
          />
        </div>
        <Row>
          <Col s={6}>
            <h5>
              Total: £{this.props.basket.basketTotal}
            </h5>
          </Col>
          <Col>
            <Link to="basket/checkout">
              <Button>
                Checkout
              </Button>
            </Link>
          </Col>
        </Row>
      </div>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    basket: state.basket,
    basketTotal: state.basketTotal
  };
}
export default connect(mapStateToProps, removeFromBasket)(BasketBar);
