import React, {Component} from 'react';
import {Row,Col} from 'react-materialize';
import './CSS/ProductList.css';
import ProductCard from './ProductCard';

const ProductList = ({ products }) => {
  const productArary = Object.keys(products).map(function(key, index) {
    return (
          <ProductCard  key={index} i={index} product={products[key]}/>
    )
  });


  return (
    <div className="listContainer">
      {productArary}
    </div>
  )
};



export default ProductList;
