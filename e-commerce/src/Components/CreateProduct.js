import React, { Component } from 'react';
import { connect } from 'react-redux';
import {Row, Input, Col, Icon, Button} from 'react-materialize';
import Header from './Header';
import { bindActionCreators } from 'redux';
import * as classActions from '../Actions/AddProductAction';
import Chocolate from '../Resources/Images/chocolate.png.png'
import Cookies from '../Resources/Images/Cookies.png.png';
import Doughnut from '../Resources/Images/Doughnut.png.png';
import Sweets from '../Resources/Images/Sweets.png.png';
import Coffee from '../Resources/Images/Coffee.png.png';
import Tea from '../Resources/Images/Tea.png.png';
import CocaCola from '../Resources/Images/pepsi.png.png';
import Joystick from '../Resources/Images/Joystick.png.png';
import Mouse from '../Resources/Images/Mouse.png.png';
import './CSS/AdminView.css'; 


class CreateProduct extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: "",
      description: "",
      price: 0.00,
      image: "",
      category: "",
      stock: 0
    };

    this.onChange = this.onChange.bind(this);
    this.onClick = this.onClick.bind(this);

  }

  onChange(e) {
    console.log(e.target.value);
    this.setState({[e.target.name]: e.target.value})
  }

  onClick(e) {
    //e.preventDefault();
    const newProduct = {
      Category: this.state.category,
      Name: this.state.name,
      Description: this.state.description,
      Price: this.state.price,
      Image: this.state.image,
      InStock: this.state.stock
    }
    this.props.actions.addProducts(newProduct);
    this.setState(
      {
        name: "",
        description: "",
        price: 0.00,
        image: "",
        category: "",
        stock: 0
      }
    )
  }

  render() {
    return (
      <div>
        <div className="productForm">
          <Row className="center">
            <h3 s={3}>Add Products</h3>
          </Row>
          <div className="createProduct">
            <Row>
              <Input
                onChange={this.onChange}
                name='name'
                value={this.state.name}
                placeholder="Name"
                s={11}>
                <Icon>face</Icon>
              </Input>
            </Row>
            <Row>
              <Input
                onChange={this.onChange}
                name='description'
                value={this.state.description}
                type='textarea'
                placeholder="Description"
                s={11}>
                <Icon>description</Icon>
              </Input>
            </Row>
            <Row>
              <Input
                onChange={this.onChange}
                name='price'
                value={this.state.price}
                placeholder="Price"
                s={11}>
                <Icon>attach_money</Icon>
              </Input>
            </Row>
            <Row>
              <Input
                s={11}
                onChange={this.onChange}
                name="image"
                value={this.state.image}
                type='select'
                label="Select Image"
                icon="insert_photo">
                  <option value={Chocolate}>Chocolate</option>
                  <option value={Cookies}>Cookies</option>
                  <option value={Doughnut}>Doughnut</option>
                  <option value={Sweets}>Sweets</option>
                  <option value={Coffee}>Coffee</option>
                  <option value={Tea}>Tea</option>
                  <option value={CocaCola}>CocaCola</option>
                  <option value={Joystick}>Joystick</option>
                  <option value={Mouse}>Mouse</option>
              </Input>
            </Row>
            <Row>
              <Input
                s={11}
                onChange={this.onChange}
                name="category"
                value={this.state.category}
                type='select'
                label="Select Category"
                icon="storage">
                  <option value='Food'>Snacks</option>
                  <option value='Beverages'>Drinks</option>
                  <option value='Games'>Games</option>
              </Input>
            </Row>
            <Row>
              <Input
                onChange={this.onChange}
                name='stock'
                value={this.state.stock}
                placeholder="Stock"
                s={11}>
                <Icon>assessment</Icon>
              </Input>
            </Row>
            <div>
              <Button
                onClick={this.onClick}
                floating
                large
                className='red'
                waves='light'
                icon='add'
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(
  state => ({
    state: state.products
  }),
  dispatch => ({
    actions: bindActionCreators(classActions, dispatch),
  }),
 )(CreateProduct);
