import React, { Component } from "react";
import { connect } from "react-redux";
import { fetchProducts } from "../Actions/AddProductAction";
import { addStock } from "../Actions/AddProductAction";
import { minusStock } from "../Actions/AddProductAction";
import { removeStock } from "../Actions/AddProductAction";
import { Table } from "react-materialize";
import "./CSS/AdminView.css";

const TableRow = ({
  id,
  name,
  category,
  price,
  inStock,
  onAdd,
  onMinus,
  onRemove
}) => {
  return (
    <tr key={id}>
      <td>{name}</td>
      <td>{category}</td>
      <td>{`£${price}`}</td>
      <td>
        <button onClick={() => onAdd(id, inStock)} className="quantityBtn">
          +
        </button>
        {inStock}
        <button onClick={() => onMinus(id, inStock)} className="quantityBtn">
          -
        </button>
      </td>
      <td>
        <button onClick={() => onRemove(id)} className="quantityBtn">
          X
        </button>
      </td>
    </tr>
  );
};

class StockTable extends Component {
  constructor(props) {
    super(props);

    this.stockAdd = this.stockAdd.bind(this);
    this.stockMinus = this.stockMinus.bind(this);
  }

  componentDidMount() {
    this.props.fetchProducts();
  }

  stockAdd = (id, inStock) => {
    console.log(id);
    this.props.addStock(id, inStock);
  };

  stockMinus = (id, inStock) => {
    console.log(id);
    this.props.minusStock(id, inStock);
  };

  stockRemove = id => {
    console.log(id);
    this.props.removeStock(id);
  };

  render() {
    const products = this.props.products;

    const productRows = Object.keys(products).map(key => {
      return (
        <TableRow
          id={key}
          name={products[key].Name}
          category={products[key].Category}
          price={products[key].Price}
          inStock={products[key].InStock}
          onAdd={this.stockAdd}
          onMinus={this.stockMinus}
          onRemove={this.stockRemove}
        />
      );
    });

    return (
      <div className="stockTable">
        <Table>
          <thead>
            <tr>
              <th data-field="name">Name</th>
              <th data-field="category">Category</th>
              <th data-field="price">Price</th>
              <th data-field="quantity">Quantity</th>
              <th data-field="remove">Remove</th>
            </tr>
          </thead>

          <tbody>{productRows}</tbody>
        </Table>
      </div>
    );
  }
}

const mapStateToProps = ({ products }) => {
  return {
    products
  };
};

export default connect(
  mapStateToProps,
  { fetchProducts, addStock, minusStock, removeStock }
)(StockTable);
