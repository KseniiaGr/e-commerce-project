import React, { Component } from "react";
import { Row, Input, Icon} from "react-materialize";
import "./CSS/FormBar.css";
import FeedBackForm from './FeedBackForm';

class FormBar extends Component {

  constructor(props){
    super(props);
    this.state = {
      displayNavBar: "FormBar",
      spinning: "noStyle spinningIn",
    }
  }


  handleClose = () => {
    // Add exit animation
    this.setState(
      {
        displayNavBar: "FormBar Exit",
        spinning: "noStyle spinningOut"
      }
    )
    setTimeout(this.closing, 900);
  };

  closing = () =>{
    this.props.handleFormBar()
  }

  render() {
    return (
      <div className={this.state.displayNavBar}>
        <Row className="closeButton">
          <a onClick={this.handleClose} className="noStyle">
            <Icon className={this.state.spinning} medium>
              close
            </Icon>
          </a>
        </Row>
        <div className="input">
          <FeedBackForm />
        </div>
      </div>
    );
  }
}

export default FormBar;