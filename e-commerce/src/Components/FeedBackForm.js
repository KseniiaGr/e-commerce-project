import React, { Component } from 'react';
import { Col, Row, Icon, Input, Button } from 'react-materialize';
import { bindActionCreators } from 'redux';
import * as classActions from '../Actions/AddProductAction';
import { connect } from 'react-redux';

class FeedBackForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      email: '',
      feedback: ''
    };

    this.onChange = this.onChange.bind(this);
    this.onClick = this.onClick.bind(this);
  }

  onChange(e) {
    this.setState({[e.target.name]: e.target.value})
  }

  onClick(e) {
    const newFeedback = {
      Name: this.state.name,
      Email: this.state.email,
      Feedback: this.state.feedback
    }
    this.props.actions.addFeedback(newFeedback);
    this.setState(
      {
        name: '',
        email: '',
        feedback: ''
      }
    )
  }

  render() {
    return (
      <div>
        <Row>
          <Input 
            onChange={this.onChange}
            value={this.state.name}
            s={12} 
            name='name'
            label="Name" 
            validate>
            <Icon>account_circle</Icon>
          </Input>
          <Input 
            onChange={this.onChange}
            value={this.state.email}
            s={12} 
            name='email'
            label="Email" 
            validate type='email'>
            <Icon>email</Icon>
          </Input>   
        </Row>
        <Row>
          <Input
            onChange={this.onChange}
            value={this.state.feedback} 
            s={12}
            name='feedback'
            type="textarea"
            label="Enter your feedback here..."
            validate>
            <Icon>feedback</Icon>
          </Input>
        </Row>
        <Row>
          <Button 
            onClick={this.onClick}
            s={8}  
            waves='light'>
            Submit
          </Button>
        </Row>
      </div>
    )
  }
}

export default connect(
  state => ({
    state: state.products
  }),
  dispatch => ({
    actions: bindActionCreators(classActions, dispatch),
  }),
 )(FeedBackForm);