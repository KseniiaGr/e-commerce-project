import { FETCH_FEEDBACK } from '../Actions/Types';


 export default (state = {}, action) =>{
   switch (action.type){
     case FETCH_FEEDBACK:
     return action.payload;
   default:
     return state;
   }
 }