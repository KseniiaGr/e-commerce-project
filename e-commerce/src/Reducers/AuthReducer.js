import { SIGNED_IN } from '../Actions/Types';

const INITIAL_STATE = {
  email: '',
  password: '',
  user: null,
  error: '',
  loading: false
 };

 export default (state = INITIAL_STATE, action) =>{
   switch (action.type){
     case SIGNED_IN:
     return { ...state, ...INITIAL_STATE, user: action.payload, };
   default:
     return state;
   }
 }
