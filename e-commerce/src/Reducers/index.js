import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import Product from '../Resources/Product';
import FeedbackReducer from './FeedbackReducer';
import AuthReducer from './AuthReducer';
import BasketReducer from './BasketReducer';

const rootReducer = combineReducers({
  auth: AuthReducer,
  products: Product,
  feedback: FeedbackReducer,
  basket: BasketReducer
});

export default rootReducer;
