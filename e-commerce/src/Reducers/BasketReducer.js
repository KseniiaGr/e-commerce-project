import _ from 'lodash';
import { ADD_TO_BASKET, REMOVE_FROM_BASKET } from '../Actions/Types';

const INITIAL_STATE = {
  basket: [],
  basketTotal: 0,
};

 export default (state = INITIAL_STATE, action) => {
   switch (action.type){
     case ADD_TO_BASKET:
      const price = action.product.Price;
      let id = 0;
      id = state.basket.length + 1;
      const { basketTotal } = state;
      const total = parseFloat(basketTotal) + parseFloat(price);
      return {
        ...state,
        basket: [...state.basket, { ...action.product, id }],
        basketTotal: total,
      };
    case REMOVE_FROM_BASKET:
      const newState = { ...state }
      _.remove(newState.basket, (item) => ((item.Code === action.item.Code) && (item.id === action.item.id)));
      newState.basketTotal = parseFloat(newState.basketTotal) - parseFloat(action.item.Price);
      return newState;

    default:
      return state;
   }
 }
